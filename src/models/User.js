'use strict'

const model = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        username: DataTypes.STRING,
        email: DataTypes.STRING
    }, {
        underscored: true
    })

    User.associate = (db) => {
        db.User.hasMany(db.Advert, {foreignKey: 'user_id'})
    }

    User.forApi = async (user) => {
        const {id, username, email} = user
        const adverts = await user.getAdverts()
        return {id, username, email, advertCount: adverts.length}
    }

  return User
}

module.exports = model
