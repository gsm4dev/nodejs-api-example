'use strict'

const model = (sequelize, DataTypes) => {
    const Advert = sequelize.define('Advert', {
        title: DataTypes.STRING,
        description: DataTypes.TEXT,
        price: DataTypes.INTEGER,
    }, {
        underscored: true,
    })

    Advert.associate = db => {
        db.Advert.belongsTo(db.Category, {foreignKey: 'category_id'})
        db.Advert.belongsTo(db.User, {foreignKey: 'user_id'})
    }

    Advert.forApi = async (advert, foruser) => {
        const {id, title, description, price} = advert
        const formattedPrice = '' + price / 100 + (0 === price % 100 ? '.00' : '')
        const category = await advert.getCategory()
        const user = foruser ? foruser : await advert.getUser()
        return {
            id,
            title,
            description,
            price: formattedPrice,
            category: category.name,
            author: user.username
        }
    }

    return Advert
}

module.exports = model
