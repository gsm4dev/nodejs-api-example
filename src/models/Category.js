'use strict'

const model = (sequelize, DataTypes) => {
    const Category = sequelize.define('Category', {
        name: DataTypes.STRING,
    },{
        tableName: 'categories',
        underscored: true,
    })

    Category.associate = (db) => {
        db.Category.hasMany(db.Advert, {foreignKey: 'category_id'})
    }

    Category.forApi = (category) => {
        const {id, name} = category
        return {id, name}
    }

    return Category
}

module.exports = model
