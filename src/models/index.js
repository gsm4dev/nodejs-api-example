const fs = require('fs')
'use strict'

const path = require('path')
const Sequelize = require('sequelize')

const env = process.env.NODE_ENV || 'development'
const dbConfig = require('../../config/db')[env]

const sequelize = new Sequelize(dbConfig.name, dbConfig.user, dbConfig.password, dbConfig)
const db = { Sequelize, sequelize }

const basename = path.basename(module.filename)

const onlyModels = file => {
    return file.indexOf('.') !== 0 &&
      file !== basename &&
      file.slice(-3) === '.js'
}

const importModel = file => {
    const modelPath = path.join(__dirname, file)
    const model = sequelize.import(modelPath)
    db[model.name] = model
}

const associate = modelName => {
    if (typeof db[modelName].associate === 'function')
      db[modelName].associate(db)
}

fs.readdirSync(__dirname)
    .filter(onlyModels)
    .forEach(importModel)

Object.keys(db).forEach(associate);

module.exports = db
