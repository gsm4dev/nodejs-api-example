'use strict'

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('users', [{
            id: 1,
            username: 'JeanLuc',
            email: 'captain@enterprise.com',
            created_at: new Date('2019-03-15 14:06:37'),
            updated_at: new Date('2019-03-15 14:06:37'),
        }, {
            id: 2,
            username: 'BenSisko',
            email: 'captain@deepspace9.com',
            created_at: new Date('2019-03-15 14:07:36'),
            updated_at: new Date('2019-03-15 14:07:36'),
        }, {
            id: 3,
            username: 'Founders',
            email: 'enemy@dominion.com',
            created_at: new Date('2019-03-15 14:08:13'),
            updated_at: new Date('2019-03-15 14:08:13'),
        }], {})
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('users', null, {})
    }
}
