'use strict'

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('categories', [{
            id: 1,
            name: 'Pets',
            created_at: new Date(),
            updated_at: new Date(),
        }, {
            id: 2,
            name: 'Cars',
            created_at: new Date(),
            updated_at: new Date(),
        }, {
            id: 3,
            name: 'Property',
            created_at: new Date(),
            updated_at: new Date(),
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('categories', null, {});
    }
}
