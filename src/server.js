'use strict'

const Boom = require('@hapi/boom')
const Hapi = require('@hapi/hapi')
const routes = require('./routes')

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
});

const server = Hapi.server({
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'localhost',
    routes: {
        validate: {
            failAction: async (req, response, err) => {
                throw Boom.badRequest(err.message)
            }
        }
    }
});

const init = async () => {

    routes.register(server)

    await server.initialize();
    return server
};

const start = async () => {

    routes.register(server)

    await server.start();
    console.log('Server running on %ss', server.info.uri)
    return server
};

module.exports = {init, start}
