'use strict'

const Boom = require('@hapi/boom')
const Joi = require('@hapi/joi')
const db = require('./models')

function registerRoutes(server) {
    server.route({
        method: 'GET',
        path: '/',
        handler: () => {
            return {message: 'Hello world!'}
        }
    })

    // Category
    server.route([{
        method: 'GET',
        path: '/api/v1/categories',
        handler: () => {
            return db.Category.findAll()
                .then(categories => {
                    return categories.map(category => db.Category.forApi(category))
                })
        }
    },

    {
        method: 'GET',
        path: '/api/v1/categories/{id}',
        handler: async (request) => {
            const category = await db.Category.findByPk(request.params.id)

            if (!category) {
                throw Boom.notFound()
            }

            return db.Category.forApi(category)
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/v1/categories',
        handler: (request) => {
            const category = db.Category.build(request.payload.category)
            return category.save()
                .then(category => db.Category.forApi(category))
        },
        options: {
            validate: {
                payload: {
                    category: {
                        name: Joi.string().min(1).max(255)
                    }
                }
            }
        }
    },

    {
        method: ['PUT', 'PATCH'],
        path: '/api/v1/categories/{id}',
        handler: async (request) => {
            const category = await db.Category.findByPk(request.params.id)

            if (!category) {
                throw Boom.notFound()
            }

            return category.update(request.payload.category)
                .then(category => db.Category.forApi(category))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                },
                payload: {
                    category: {
                        name: Joi.string().min(1).max(255)
                    }
                }
            }
        }
    },

    {
        method: 'DELETE',
        path: '/api/v1/categories/{id}',
        handler: async (request) => {
            const category = await db.Category.findByPk(request.params.id)

            if (!category) {
                throw Boom.notFound()
            }

            return category.destroy()
                .then(category => db.Category.forApi(category))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    }])

    // User
    server.route([
    {
        method: 'GET',
        path: '/api/v1/users',
        handler: () => {
            return db.User.findAll()
                .then(users => {
                    return users.map(user => db.User.forApi(user))
                })
        }
    },

    {
        method: 'GET',
        path: '/api/v1/users/{id}',
        handler: async (request) => {
            const user = await db.User.findByPk(request.params.id)

            if (!user) {
                throw Boom.notFound()
            }

            return db.User.forApi(user)
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/v1/users',
        handler: (request) => {
            const user = db.User.build(request.payload.user)
            return user.save()
                .then(user => db.User.forApi(user))
        },
        options: {
            validate: {
                payload: {
                    user: {
                        username: Joi.string().min(1).max(255),
                        email: Joi.string().min(1).max(255)
                    }
                }
            }
        }
    },

    {
        method: ['PUT', 'PATCH'],
        path: '/api/v1/users/{id}',
        handler: async (request) => {
            const user = await db.User.findByPk(request.params.id)

            if (!user) {
                throw Boom.notFound()
            }

            return user.update(request.payload.user)
                .then(user => db.User.forApi(user))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                },
                payload: {
                    user: {
                        username: Joi.string().min(1).max(255),
                        email: Joi.string().min(1).max(255)
                    }
                }
            }
        }
    },

    {
        method: 'DELETE',
        path: '/api/v1/users/{id}',
        handler: async (request) => {
            const user = await db.User.findByPk(request.params.id)

            if (!user) {
                throw Boom.notFound()
            }

            return user.destroy()
                .then(user => db.User.forApi(user))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/v1/users/{id}/adverts/{latest?}',
        handler: async (request) => {
            const user = await db.User.findByPk(request.params.id)
            const islatest = request.params.latest === 'latest'
            let filter

            if (!user) {
                throw Boom.notFound('No user found')
            }

            if (islatest) {
                filter = {
                    limit: 1,
                    order: [['updated_at', 'desc']]
                }
            }

            const adverts = await user.getAdverts(filter)
                .then(adverts => {
                     return adverts.map(async advert => db.Advert.forApi(advert, user))
                })
                .all()

            if (!adverts.length) {
                if (!islatest) {
                    return []
                }

                return Boom.notFound()
            }

            return islatest ? adverts[0] : adverts
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    }

    ])

    // Advert
    server.route([{
        method: 'GET',
        path: '/api/v1/adverts',
        handler: () => {
            return db.Advert.findAll()
                .then(adverts => {
                    return adverts.map(async advert => db.Advert.forApi(advert))
                })
                .all()
        }
    },

    {
        method: 'GET',
        path: '/api/v1/adverts/{id}',
        handler: async (request) => {
            const advert = await db.Advert.findByPk(request.params.id)

            if (!advert) {
                throw Boom.notFound()
            }

            return db.Advert.forApi(advert)
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/v1/adverts',
        handler: (request) => {
            const advert = db.Advert.build(request.payload.advert)
            return advert.save()
                .then(advert => db.Advert.forApi(advert))
        },
        options: {
            validate: {
                payload: {
                    user: {
                        title: Joi.string().min(1).max(255).required(),
                        description: Joi.string().min(1).required(),
                        price: Joi.number().positive().required(),
                        category_id: Joi.number().integer().required(),
                        user_id: Joi.number().integer().required(),
                    }
                }
            }
        }
    },

    {
        method: ['PUT', 'PATCH'],
        path: '/api/v1/adverts/{id}',
        handler: async (request) => {
            const advert = await db.Advert.findByPk(request.params.id)

            if (!advert) {
                throw Boom.notFound()
            }

            return advert.update(request.payload.advert)
                .then(advert => db.Advert.forApi(advert))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                },
                payload: {
                    user: {
                        title: Joi.string().min(1).max(255).required(),
                        description: Joi.string().min(1).required(),
                        price: Joi.number().positive().required(),
                        category_id: Joi.number().integer().required(),
                        user_id: Joi.number().integer().required(),
                    }
                }
            }
        }
    },

    {
        method: 'DELETE',
        path: '/api/v1/adverts/{id}',
        handler: async (request) => {
            const advert = await db.Advert.findByPk(request.params.id)

            if (!advert) {
                throw Boom.notFound()
            }

            return advert.destroy()
                .then(advert => db.Advert.forApi(advert))
        },
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().positive().required()
                }
            }
        }
    }])

}

module.exports = {
    register: registerRoutes
}
