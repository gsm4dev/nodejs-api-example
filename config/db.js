module.exports = {
    development: {
        dialect: 'sqlite',
        storage: 'data/dev-db.sqlite3'
    },
    test: {
        dialect: 'sqlite',
        storage: 'data/test-db.sqlite3'
    },
    production: {
        dialect: 'mysql',
        database: 'ebdb',
        username: process.env.RDS_USERNAME,
        password: process.env.RDS_PASSWORD,
        port: process.env.RDS_PORT,
        host: process.env.RDS_HOSTNAME,
    }
}
