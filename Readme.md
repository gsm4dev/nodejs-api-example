## Objective

Build an API that manages adverts, which could be used to retrieve and store information for a classifieds website.

Please spend no more than 3 hours on this task.

## Languages & Technologies

PHP or NodeJS. There are no other tech limitations. Use whichever technology you need to use to achieve the
objective.

## Data

You will have received CSV’s. This is the structure of the data and the data that you are to use in any way you see
fit. You may use the CSV’s as the data source or import those somehow, that part is up to you.

### What is an advert?

Users post adverts in order to sell goods or services.

### Categories

Each advert is assigned to one category only. The categories are; pets, cars, and property.


## API capabilities

- Retrieve all categories
- Retrieve one category
- Retrieve one advert
- Retrieve all adverts
- Retrieve one user
- Retrieve a users latest advert
- Retrieve all of a users adverts

You don’t have to do all of these. Think about which ones you assess are the most important. You should look to
build three of these within the time limit.


### Implementation notes

To populate example data, run the following commands:

    npx sequelize db:migrate
    npx sequelize db:seed:all
