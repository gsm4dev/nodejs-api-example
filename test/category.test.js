'use strict'

const Lab = require('@hapi/lab')
const { expect } = require('@hapi/code')
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script()
const { init } = require('../src/server')

let server
init().then(running => server = running)


describe('GET /api/v1/categories', () => {
    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/categories'
        })

        expect(res.statusCode).to.equal(200)
    })
})

describe('GET /api/v1/categories/2', () => {
    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/categories/2'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('responds with object', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/categories/2'
        })

        const expected = {
            id: 2,
            name: 'Pets'
        }

        expect(res.body).to.equal(expected)
    })
})
