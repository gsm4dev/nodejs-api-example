'use strict'

const Lab = require('@hapi/lab')
const { expect } = require('@hapi/code')
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script()
const { init } = require('../src/server')

let server
init().then(running => server = running)

describe('GET /', () => {
    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        })
        expect(res.statusCode).to.equal(200)
    })

    it('responds with object', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        })

        const expected = {
            message: 'Hello world!'
        }
        expect(JSON.parse(res.payload)).to.equal(expected)
    })

})

describe('GET /bad', () => {
    it('responds with 404', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/bad'
        })
        expect(res.statusCode).to.equal(404)
    })
})
